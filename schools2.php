<?php
//error_reporting(0); /* stops php errors */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$id = "";
$country = "";
switch ($_SERVER['REQUEST_URI']) {                  
    case "/israel-beer-sheva/ben-gurion/":                                            
        $id = "51"; 
        $country = "Israel";  
        break;
    case "/india-nellore/narayana/":                                          
        $id = "2184";
        $country = "India";
        break;
    case "/malaysia-george-town/penang/":                                         
        $id = "448";
        $country = "Malaysia";
        break; 
    case "/lebanon-beirut/american-university/":                                         
        $id = "1871";
        $country = "Lebanon";
        break;  
    case "/india-mangalore/kasturba/":                                         
        $id = "1636";
        $country = "India";
        break;  
    case "/chile-santiago/pontificia/":                                         
        $id = "378";
        $country = "Chile";
        break;
    case "/bahrain-manama/rcsi/":                                         
        $id = "2248";
        $country = "Bahrain";
        break;
    case "/ireland-dublin/rcsi/":                                         
        $id = "1729";
        $country = "Ireland";
        break;
    case "/colombia-medellin/ces/":                                         
        $id = "1831";
        $country = "Colombia";
        break;
    case "/mexico-mexico-city/unam/":                                         
        $id = "1589";
        $country = "Mexico";
        break;
    case "/indonesia-yogyakarta/gadjah-mada/":                                         
        $id = "905";
        $country = "Indonesia";
        break;
    case "/uae-sharjah/university-medicine/":                                         
        $id = "2321";
        $country = "United Arab Emirates";
        break;
    case "/qatar-doha/weill-cornell/":                                         
        $id = "849";
        $country = "Qatar";
        break;
    case "/nepal-pokhara/mcoms/":                                         
        $id = "402";
        $country = "Nepal";
        break;
    case "/brazil-campo-grande/ufms/":                                         
        $id = "256";
        $country = "Brazil";
        break;
    case "/mexico-chihuahua/uach/":                                         
        $id = "1093";
        $country = "Mexico";
        break;
    case "/portugal-braga/uminho/":                                         
        $id = "720";
        $country = "Portugal";
        break;      
    case "/colombia-bogota/fucs/":                                         
        $id = "570";
        $country = "Colombia";
        break;  
    case "/uganda-kampala/ceha/":                                         
        $id = "3543";
        $country = "Uganda";
        break;  
    case "/ireland-galway/nui/":                                         
        $id = "1730";
        $country = "Ireland";
        break;  
    case "/italy-pavia/univ-pavia/":                                         
        $id = "1735";
        $country = "Italy";
        break;  
    case "/india-dharwad/sdm/":                                         
        $id = "2204";
        $country = "India";
        break;  
    case "/india-ludhiana/dmc/":                                         
        $id = "37";
        $country = "India";
        break;  
    case "/india-kolenchery/malankara/":                                         
        $id = "2209";
        $country = "India";
        break;  
    case "/colombia-medellin/upb/":                                         
        $id = "504";
        $country = "Colombia";
        break;          
    case "/antigua-barbuda-coolidge/aua/":                                         
        $id = "993";
        $country = "Antigua and Barbuda";
        break;  
    case "/india-manipal/kmc/":                                         
        $id = "1453";
        $country = "India";
        break;
    case "/india-new-delhi/mime/":                                         
        $id = "3755";
        $country = "India";
        break;  
    case "/nigeria-ile-ife/oauchs/":                                         
        $id = "1858";
        $country = "Nigeria";
        break;  
    case "/usa-ny/pagny/":                                         
        $id = "3542";
        $country = "United States";
        break;  
    case "/pakistan-lahore/shalamar/":                                        
        $id = "2454";
        $country = "Pakistan";
        break;
    case "/philippines-manila/upmcm/":                                        
        $id = "1819";
        $country = "Philippines";
        break;  
    case "/egypt-cairo/ain-shams/":                                        
        $id = "337";
        $country = "Egypt";
        break;
    case "/rwanda-butare/urcmhs/":                                        
        $id = "227";
        $country = "Rwanda";
        break;
    case "/malaysia-kuala-lumpur/imu/":                                        
        $id = "1687";
        $country = "Malaysia";
        break;
    case "/usa-pa/skmc/":                                         
        $id = "121";
        $country = "United States";
        break;
    case "/malaysia-petaling-jaya/segi/":                                         
        $id = "3620";
        $country = "Malaysia";
        break;
    case "/ethiopia-addis-ababa/sphmmc/":                                         
        $id = "3142";
        $country = "Ethiopia";
        break;
    case "/chile-santiago/udd/":                                         
        $id = "1643";
        $country = "Chile";
        break;
    case "/kenya-nairobi/kusom/":                                         
        $id = "2766";
        $country = "Kenya";
        break;  
    case "/india-amristar/sgrdi/":                                         
        $id = "325";
        $country = "India";
        break;  
    case "/uganda-ishaka/kiufmd/":                                         
        $id = "2565";
        $country = "Uganda";
        break;
    case "/dominican-republic-santo-domingo/unibe/":                                         
        $id = "1836";
        $country = "Dominican Republic";
        break;      
    case "/georgia-tbilisi/tmafom/":                                         
        $id = "571";
        $country = "Georgia";
        break;
    case "/brazil-lajeado/univates/":                                         
        $id = "4636";
        $country = "Brazil";
        break;      
    case "/nepal-tansen/lumbini/":                                         
        $id = "2538";
        $country = "Nepal";
        break;      
    case "/australia-burwood-victoria/deakin/":                                         
        $id = "2028";
        $country = "Australia";
        break;                          
    case "/zambia-ndola/adch/":                                         
        $id = "3403";
        $country = "Zambia";
        break;  
    case "/malawi-mzuzu/echs/":                                         
        $id = "4173";
        $country = "Malawi";
        break;
    case "/kenya-nairobi/jomo/":                                         
        $id = "4170";
        $country = "Kenya";
        break;
    case "/uganda-kabale/kabale/":                                         
        $id = "5885";
        $country = "Uganda";
        break;
    case "/uganda-kampala/kiu/":                                         
        $id = "4181";
        $country = "Uganda";
        break;
    case "/malawi-lilongwe/kamuzu/":                                         
        $id = "4174";
        $country = "Malawi";
        break; 
    case "/kenya-nairobi/kenyatta/":                                         
        $id = "4171";
        $country = "Kenya";
        break;
    case "/ghana-kumasi/knust/":                                         
        $id = "4169";
        $country = "Ghana";
        break;
    case "/zambia-lusaka/lsnm/":                                         
        $id = "4188";
        $country = "Zambia";
        break; 
    case "/mozambique-nampula/lurio/":                                         
        $id = "3489";
        $country = "Zambia";
        break;
    case "/uganda-kampala/mepi/":                                         
        $id = "4183";
        $country = "Uganda";
        break;
    case "/uganda-kampala/nepi/":                                         
        $id = "4184";
        $country = "Uganda";
        break;
    case "/uganda-kampala/mildmay/":                                         
        $id = "5949";
        $country = "Uganda";
        break;
    case "/tanzania-dar-es-salaam/muhas/":                                         
        $id = "4180";
        $country = "Tanzania";
        break;
    case "/sierra-leone-southern-province/bo/":                                         
        $id = "5945";
        $country = "Sierra Leone";
        break; 
    case "/south-africa-cape-town/su/":                                         
        $id = "230";
        $country = "South Africa";
        break;
    case "/zambia-lusaka/uzambia/":                                         
        $id = "4190";
        $country = "Zambia";
        break;
    case "/nigeria-ibadan/ison/":                                         
        $id = "5947";
        $country = "Nigeria";
        break;
    case "/south-africa-durban/ukn/":                                         
        $id = "4178";
        $country = "South Africa";
        break;
    case "/zimbabwe-harare/uz/":                                         
        $id = "4191";
        $country = "Zimbabwe";
        break;
    case "/south-africa-johannesburg/sysmex/":                                         
        $id = "5948";
        $country = "South Africa";
        break;
    case "/nigeria-ibadan/icom/":                                         
        $id = "148";
        $country = "Nigeria";
        break;      
    default:                                            
        break;  
}

$xml = simplexml_load_file('https://www.gemxelectives.org/xml/gemx.xml');
$idnum = (int)$id; /* for xml nodes */
/* initialize variable for pages (like partner schools) not using last updated */
$lastupdated = ""; 

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if !(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>
        <?php
            $getId = $xml->xpath('/schools/school[@id='.$idnum.']');
            echo "GEMx Partner Schools - " . $getId[0]->name . " | " . $country;
        ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:image" content="../images/180profile-gemx.gif" />

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php require ("header.php"); ?>
    <div class="six schools">      
        <div class="main">
            <div class="sliders">
                <?php include ("sliders2.php"); ?>
            </div><!-- close sliders -->
            <?php 
                $getId = $xml->xpath('/schools/school[@id='.$idnum.']'); /* this line is printed once in the file and determines which id defines the attributes */
                echo "<h2 class='name'>" . $getId[0]->name . "</h2>";
                echo "<p>" . $getId[0]->description . "</p>";
            ?>
            <div class="disclaimer above">
                <p>
                    <strong>Note:</strong> The information for this institution, including the description and the electives it offers, is provided and maintained by the institution. This information is not verified by ECFMG. Interested individuals are encouraged to verify the accuracy and completeness of this information directly with the institution. 
                </p>
            </div>
        </div><!-- closes main -->
        <div class="sidebar">
            <div class="maps">
                <?php include ("maps2.php"); ?>
            </div><!-- close maps -->
            <div class="language">
                <h3>Language of Instruction:</h3>           
                <?php 
                    $i = 0;
                    foreach ($getId[0]->languages->language as $countlanguage) { /* this foreach loop counts the languages and gets a total to be used for the second foreach */
                        $i++;
                        $total = $i;
                    }
                    echo "<p>";
                    $l = 0;
                    foreach ($getId[0]->languages->language as $showlanguage) { /* foreach for printing languages comes after 'counter' foreach loop */
                        $l++;
                        if ($total > $l) { /* if total is greater than $l, then SHOW the comma because there are MORE languages to printed */
                            echo "<span>" . $showlanguage . ", </span>";
                        } else { /* if total is not greater than $l, then HIDE the comma because there are NO MORE languages to printed */
                            echo "<span>" . $showlanguage . "</span>";    
                        }                       
                    }
                ?>
                </p>
            </div><!-- closes language div -->

            <div class="electives">
                <h3>Exchange Electives Offered in the Following Specialties:</h3>
                <ul>
                <?php 
                    foreach ($getId[0]->electives->elective as $showelective) {
                        echo "<li>" . $showelective . "</li>";
                    }
                ?>
                </ul>
            </div><!-- closes electives div -->

            <div class="schoollink interior">
                <a href="https://students.gemxelectives.org/" target="_blank">
                    <div class="ilink">
                        <img src="../../images/partner-schools-students-graphic.gif" alt="">
                        <h6>Students</h6>
                        <p>Apply Now or Ask Your School to Join GEMx</p>
                    </div>
                </a>
                <a href="../../join-partnership/" target="_blank">
                    <div class="ilink">
                        <img src="../../images/partner-schools-schools-graphic.gif" alt="">
                        <h6>Institutions</h6>
                        <p>Learn More About Being a Partner</p>
                    </div>
                </a>
            </div><!-- closes schoollink div -->
            <div class="schoollink exterior">
                <h5>External Links</h5>
                <?php 
                $facebookurl = $getId[0]->facebook;

                if(empty($facebookurl)) {
                    // if <facebook> in xml is empty, show nothing 
                } else { // else show the linked graphics and text
                ?>

                <a href="<?php echo $getId[0]->facebook; ?>" target="_blank">
                    <div class="elink">
                        <img src="../../images/partner-school-facebook-graphic.gif" alt="GEMx Partners Schools on Facebook" />
                        <h6>Institution Facebook</h6>
                    </div>
                </a>

                <?php } 

                $schoolurl = $getId[0]->url;

                if(empty($schoolurl)) {
                    // if <url> in xml is empty, show nothing 
                } else { // else show the linked graphics and text ?>
                
                    <a href="<?php echo $getId[0]->url; ?>" target="_blank">
                        <div class="elink">
                            <img src="../../images/partner-school-website-graphic.gif" alt="GEMx Partners Schools Websites" />
                            <h6>Institution Website</h6>
                        </div>
                    </a>

                <?php } ?>
            </div><!-- closes facebook div -->
            <div class="disclaimer below">
            <p>
                <strong>Note:</strong> The information for this institution, including the description and the electives it offers, is provided and maintained by the institution. This information is not verified by ECFMG. Interested individuals are encouraged to verify the accuracy and completeness of this information directly with the institution. 
            </p>
        </div>
        </div><!-- closes sidebar div -->
    </div><!-- closes six schools div -->
    <!--<div class="six columns update">Last updated April 1, 2015.</div>-->
<?php 
//var_dump($schoolist);
require ("footer.php"); ?>

